var app = angular.module("Elements", []);

angular.module('Elements').filter('twoAfterDot', function () {
    return function (number) {
        if (!number) return 0;
        return number.toFixed(2);
    }
});

app.controller('ElementsController', ['$scope', '$http',
    function($scope, $http) {

        $scope.selected_unit = '';
        $scope.selected_elem = [];
        $scope.ppms = [45, 53, 208794.9, 4, 45, 53];
        $scope.conversions = {};

        $http.get('data/Units.json').success(function(data) {
            $scope.units = data;
            $scope.selected_unit = $scope.units[0];
        });

        $http.get('data/Elements.json').success(function(data) {
            $scope.elements = data;
            var result = [];
            $scope.elements.forEach(function(elem) {
                if (elem['ParentElementId'] == 'NULL' && result.indexOf(elem) == -1) {
                    result.push(elem);
                    $scope.conversions[elem['ElementId'] + '-1'] = 1;
                }
            });

            $scope.elementsTree = [];
            result.forEach(function (parent){
                var child_data = [];
                parent['ParentElementId'] = parent['ElementId'];
                $scope.selected_elem.push(parent);
                $scope.elements.forEach(function(elem) {
                    if (parent['ElementId'] == elem['ParentElementId']) {
                        child_data.push(elem);
                    }
                });
                $scope.elementsTree.push(child_data);
            });
        });

        $http.get('data/Element Units conversation.json').success(function(data) {
            data.forEach(function(converse) {
                $scope.conversions[converse['ElementId'] + '-' + converse['ToUnitId']] = converse['ConversionValue'];
            });
        });
    }]
);